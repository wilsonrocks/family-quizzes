const assert = require('assert').strict;
const path = require('path');
const fs = require('fs');
const slugify = require('slugify');

/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(`
    query {
      allQuizzesJson(sort: { fields: date, order: ASC }) {
        edges {
          node {
            quizName
            rounds {
              instructions
              name
              questions {
                answer
                image
                question
                audio
                video
              }
            }
            date
          }
        }
      }

      allQuizzesYaml(sort: { fields: date, order: ASC }) {
        edges {
          node {
            quizName
            rounds {
              instructions
              name
              questions {
                answer
                image
                question
                audio
                video
              }
            }
            date
          }
        }
      }
    }
  `);

  const quizzes = [
    ...result.data.allQuizzesJson.edges,
    ...result.data.allQuizzesYaml.edges,
  ];

  const quizzesWithPathStubs = quizzes.map(({ node }) => {
    const sluggifiedQuizName = slugify(node.quizName, {
      strict: true,
      lower: true,
    });
    const quizPathStub = `/quiz/${sluggifiedQuizName}`;
    return {
      ...node,
      sluggifiedQuizName,
      quizPathStub,
    };
  });

  // create the index page
  createPage({
    path: '/',
    component: path.resolve('./src/templates/index.jsx'),
    context: { quizzes: quizzesWithPathStubs },
  });

  // create the list of all quizzes page
  createPage({
    path: '/quizzes',
    component: path.resolve('./src/templates/quizzes.jsx'),
    context: { quizzes: quizzesWithPathStubs },
  });

  // handle each quiz
  quizzesWithPathStubs.forEach((quiz, index) => {
    const { quizName, date, rounds, quizPathStub, sluggifiedQuizName } = quiz;
    const numberOfRounds = rounds.length;
    assert(
      date.match(/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/),
      `Quiz ${quizName} date field, equal to ${date}, is not of the form YYYY-MM-DD`
    );

    const quizNumber = index + 1;

    // make intro page for the quiz
    createPage({
      path: quizPathStub,
      component: path.resolve('./src/templates/intro.jsx'),
      context: {
        quizNumber,
        sluggifiedQuizName,
        quizName,
        date,
        rounds,
        quizPathStub,
      },
    });

    // make round intro pages

    rounds.forEach((round, index) => {
      const { name, instructions } = round;
      const roundNumber = index + 1;

      // make round intro page
      createPage({
        path: `${quizPathStub}/round${roundNumber}`,
        component: path.resolve('./src/templates/round.jsx'),
        context: {
          roundNumber,
          numberOfRounds,
          name,
          instructions,
          quizNumber,
          quizName,
          sluggifiedQuizName,
          quizPathStub,
        },
      });

      // make round finished page

      createPage({
        path: `${quizPathStub}/round${roundNumber}/finished`,
        component: path.resolve('./src/templates/finished.jsx'),
        context: {
          quizName,
          sluggifiedQuizName,
          quizPathStub,
          roundNumber,
          name,
          quizNumber,
        },
      });

      const numberOfQuestions = round.questions.length;

      // make round answers page
      const answers = round.questions.map((x) => x.answer);

      if (!round.questions)
        throw new Error(
          `quiz with name ${sluggifiedQuizName} round ${round.name} has no questions`
        );

      round.questions.forEach((question, index) => {
        const lastQuestion = index + 1 === numberOfQuestions;
        const {
          question: questionText,
          image,
          audio,
          video,
          answer,
        } = question;

        if (audio !== null && !fs.existsSync(path.join('./src/audio', audio))) {
          throw new Error(
            `audio ${audio} referenced in question ${questionText} in quiz ${sluggifiedQuizName} is not found`
          );
        }

        if (
          image !== null &&
          !fs.existsSync(path.join('./src/images', image))
        ) {
          throw new Error(
            `image ${image} referenced in question ${questionText} in quiz ${sluggifiedQuizName} is not found`
          );
        }

        if (video !== null && !fs.existsSync(path.join('./src/video', video))) {
          throw new Error(
            `video ${video} referenced in question ${questionText} in quiz ${sluggifiedQuizName} is not found`
          );
        }

        const questionNumber = index + 1;

        // make question page

        createPage({
          path: `${quizPathStub}/round${roundNumber}/question${questionNumber}`,
          component: path.resolve('./src/templates/question.jsx'),
          context: {
            quizNumber,
            roundNumber,
            name,
            questionText,
            questionNumber,
            lastQuestion,
            image,
            audio,
            video,
            quizName,
            sluggifiedQuizName,
            quizPathStub,
            answer,
          },
        });

        // make new style answer pages
        createPage({
          path: `${quizPathStub}/round${roundNumber}/answer${questionNumber}`,
          component: path.resolve('./src/templates/answer.jsx'),
          context: {
            answer,
            audio,
            image,
            lastQuestion,
            name,
            numberOfQuestions,
            numberOfRounds,
            questionNumber,
            questionText,
            quizName,
            sluggifiedQuizName,
            quizPathStub,
            quizNumber,
            roundName: name,
            roundNumber,
            video,
          },
        });
      });
    });
  });
};
