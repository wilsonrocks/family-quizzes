import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import PageLayout from '../components/PageLayout';

const Grid = styled.div`
  display: grid;
  @media (min-width: 600px) {
    grid-template-columns: 1fr 2fr 2fr;
  }
  grid-template-rows: 1fr;
  width: 100%;
`;

const Quizzes = ({ pageContext: { quizzes } }) => {
  return (
    <PageLayout>
      <h1>All of our quizzes</h1>
      <Grid>
        {quizzes.reverse().map(({ quizName, quizPathStub, rounds, date }) => (
          <>
            <span>{new Date(date).toLocaleDateString()}</span>
            <div>
              <Link to={quizPathStub}>{quizName}</Link>
            </div>
            <span>{rounds.map((round) => round.name).join(', ')} </span>
          </>
        ))}
      </Grid>
    </PageLayout>
  );
};

export default Quizzes;
