import { Link } from 'gatsby';
import React from 'react';
import PageLayout from '../components/PageLayout';



const IndexPage = ({ pageContext: { quizzes } }) => {
  const mostRecentQuiz = quizzes[quizzes.length - 1];
  return (
    <PageLayout>
      <h1>Our family quiz</h1>
      <p>
        Welcome to the archive of our family lockdown Zoom quiz! It has been
        running for over a year now.
      </p>
      <p>
        Try our most recent quiz, called{' '}
        <Link to={mostRecentQuiz.quizPathStub}>{mostRecentQuiz.quizName}</Link>,
        which features questions on:
      </p>
      <ul>
        {mostRecentQuiz.rounds.map((round, index) => (
          <li key={index}>{round.name}</li>
        ))}
      </ul>
      <p>
        or browse <Link to="/quizzes">all our quizzes</Link>. You can also{' '}
        <Link to="/about">find out more</Link> about how and why this site
        exists.
      </p>
    </PageLayout>
  );
};

export default IndexPage;
