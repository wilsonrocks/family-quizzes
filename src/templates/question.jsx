import React from 'react';
import { graphql, Link } from 'gatsby';
import Navigation from '../components/nav';
import QuizLayout from '../components/QuizLayout';
import QuizContent from '../components/QuizContent';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import NavContents from '../components/NavContents';
import styled from 'styled-components';

const QuestionText = styled.p`
  max-width: 40ch;
`;

const getBackUrl = (quizPathStub, roundNumber, questionNumber) => {
  if (questionNumber !== 1)
    return `${quizPathStub}/round${roundNumber}/question${questionNumber - 1}`;
  if (roundNumber !== 1)
    return `${quizPathStub}/round${roundNumber - 1}/finished`;
  // so now it's the first Q of the first round
  return `${quizPathStub}/round1`;
};

const Round = ({
  pageContext: {
    roundNumber,
    name: roundName,
    questionText,
    questionNumber,
    lastQuestion,
    image,
    audio,
    video,
    quizName,
    quizPathStub,
  },
  data,
}) => {
  const nextUrl = lastQuestion
    ? `${quizPathStub}/round${roundNumber}/finished`
    : `${quizPathStub}/round${roundNumber}/question${questionNumber + 1}`;

  const backUrl = getBackUrl(quizPathStub, roundNumber, questionNumber);

  return (
    <QuizLayout>
      <NavContents>
        <Navigation nextUrl={nextUrl} backUrl={backUrl} />

        <Link to={`${quizPathStub}`}>{quizName}</Link>

        <Link to={`${quizPathStub}/round${roundNumber}`}>
          Round {roundNumber} - {roundName}
        </Link>
        <span>Question {questionNumber}</span>
      </NavContents>
      <QuizContent>
        {questionText && (
          <QuestionText>
            {questionText.split('\n').flatMap((str) => [str, <br key={str} />])}
          </QuestionText>
        )}
        {image && (
          <GatsbyImage
            objectFit="contain"
            loading="eager"
            height="100px"
            image={getImage(data.image)}
            alt=""
          />
        )}
        {audio && <audio controls src={data.audio.publicURL} />}
        {video && <video controls src={data.video.publicURL} />}
      </QuizContent>
    </QuizLayout>
  );
};

export const query = graphql`
  query($image: String, $audio: String, $video: String) {
    image: file(relativePath: { eq: $image }) {
      id
      childImageSharp {
        gatsbyImageData(width: 2000)
      }
    }

    audio: file(name: {}, relativePath: { eq: $audio }) {
      id
      publicURL
    }

    video: file(name: {}, relativePath: { eq: $video }) {
      id
      publicURL
    }
  }
`;

export default Round;
