import React from 'react';
import { Link } from 'gatsby';
import QuizLayout from '../components/QuizLayout';
import QuizContent from '../components/QuizContent';
import NavContents from '../components/NavContents';

const Intro = ({ pageContext }) => {
  const { quizName, rounds, quizPathStub } = pageContext;
  const nextUrl = `${quizPathStub}/round1`;
  return (
    <QuizLayout>
      <NavContents>The navigation will go here</NavContents>
      <QuizContent>
        <div>
          <h2>{quizName}</h2>
          <div>
            <ol>
              {rounds.map((round, index) => (
                <li key={round.name}>
                  <Link to={`${quizPathStub}/round${index + 1}`}>
                    {round.name}
                  </Link>
                </li>
              ))}
            </ol>
          </div>
        </div>
      </QuizContent>
      <Link to={nextUrl}>Let&apos;s Get Started</Link>
    </QuizLayout>
  );
};

export default Intro;
