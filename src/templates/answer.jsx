import React, { useState, useEffect } from 'react';
import { graphql } from 'gatsby';

import Navigation from '../components/nav';
import styled from 'styled-components';
import QuizLayout from '../components/QuizLayout';
import QuizContent from '../components/QuizContent';
import NavContents from '../components/NavContents';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';

const Button = styled.button`
  font-family: 'Andika';
  text-transform: uppercase;
  font-size: 2rem;
`;

const RevealButton = ({ onClick }) => (
  <Button onClick={onClick}>Reveal Answer</Button>
);

const getNextUrl = ({
  numberOfQuestions,
  numberOfRounds,
  questionNumber,
  quizPathStub,
  roundNumber,
}) => {
  if (questionNumber < numberOfQuestions)
    return `${quizPathStub}/round${roundNumber}/answer${questionNumber + 1}`;
  if (roundNumber === numberOfRounds) return '/';
  return `${quizPathStub}/round${roundNumber + 1}`;
};

const Answer = ({
  pageContext: {
    answer,
    audio,
    image,
    numberOfQuestions,
    numberOfRounds,
    questionNumber,
    questionText,
    quizName,
    quizPathStub,
    roundName,
    roundNumber,
    video,
  },
  data,
}) => {
  const [hasRevealed, setHasRevealed] = useState(false);
  useEffect(() => {
    const listener = (event) => {
      if (event.key === ' ') setHasRevealed(true);
    };
    document.addEventListener('keypress', listener);
    return () => {
      document.removeEventListener('keypress', listener);
    };
  }, []);

  const backUrl =
    questionNumber === 1
      ? `${quizPathStub}/round${roundNumber}/finished`
      : `${quizPathStub}/round${roundNumber}/answer${questionNumber - 1}`;

  const nextUrl = getNextUrl({
    questionNumber,
    quizPathStub,
    roundNumber,
    numberOfQuestions,
    numberOfRounds,
  });

  return (
    <QuizLayout>
      <NavContents>
        {' '}
        <Navigation
          backUrl={backUrl}
          nextUrl={nextUrl}
          holdKeyboardNavigation={!hasRevealed}
        />
        <span>{quizName}</span>
        <span>
          Round {roundNumber} - {roundName}
        </span>
        <span>Question {questionNumber}</span>
      </NavContents>

      <QuizContent>
        {questionText && <p>{questionText}</p>}
        {image && (
          <GatsbyImage
            objectFit="contain"
            loading="eager"
            height="100px"
            image={getImage(data.image)}
            alt=""
          />
        )}
        {audio && <audio controls src={data.audio.publicURL} />}
        {video && <video controls src={data.video.publicURL} />}
        {hasRevealed ? (
          <div>{answer}</div>
        ) : (
          <RevealButton onClick={() => setHasRevealed(true)} />
        )}
      </QuizContent>
    </QuizLayout>
  );
};

export const query = graphql`
  query($image: String, $audio: String, $video: String) {
    image: file(relativePath: { eq: $image }) {
      id
      childImageSharp {
        gatsbyImageData(width: 2000)
      }
    }

    audio: file(name: {}, relativePath: { eq: $audio }) {
      id
      publicURL
    }

    video: file(name: {}, relativePath: { eq: $video }) {
      id
      publicURL
    }
  }
`;

export default Answer;
