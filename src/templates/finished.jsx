import React from 'react';
import Navigation from '../components/nav';
import QuizContent from '../components/QuizContent';
import QuizLayout from '../components/QuizLayout';
import NavContents from '../components/NavContents';

const Round = ({
  pageContext: { roundNumber, name, quizNumber, quizName, quizPathStub },
}) => {
  const nextUrl = `${quizPathStub}/round${roundNumber}/answer1`;
  return (
    <QuizLayout>
      <NavContents>
        <Navigation nextUrl={nextUrl} />
        <span>{quizName}</span>
        <span>
          Round {roundNumber} - {name}
        </span>
      </NavContents>

      <QuizContent>
        <p>Here come the answers!</p>
      </QuizContent>
    </QuizLayout>
  );
};

export default Round;
