import React from 'react';
import Navigation from '../components/nav';
import NavContents from '../components/NavContents';
import QuizContent from '../components/QuizContent';
import QuizLayout from '../components/QuizLayout';

const Round = ({
  pageContext: { name, instructions, roundNumber, quizName, quizPathStub },
}) => {
  const nextUrl = `${quizPathStub}/round${roundNumber}/question1`;
  const backUrl = `${quizPathStub}`;
  return (
    <QuizLayout>
      <NavContents>
        <Navigation backUrl={backUrl} nextUrl={nextUrl} />
        <span>{quizName}</span>
        <span>
          Round {roundNumber} - {name}
        </span>
      </NavContents>
      <QuizContent>
        <div>
          <p>
            Round {roundNumber} - {name}
          </p>
          {instructions && <p>{instructions}</p>}
        </div>
      </QuizContent>
    </QuizLayout>
  );
};

export default Round;
