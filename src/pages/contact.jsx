import React from 'react';
import PageLayout from '../components/PageLayout';

const Contact = () => (
  <PageLayout>
    <p>
      You can reach me at <address>jameswilson@hey.com</address>.
    </p>
  </PageLayout>
);

export default Contact;
