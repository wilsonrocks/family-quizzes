import { graphql, useStaticQuery } from 'gatsby';
import { twoElementsToCompare } from './random';

export const useQuizzes = () => {
  const data = useStaticQuery(graphql`
    query {
      allQuizzesJson(sort: { fields: date, order: ASC }) {
        edges {
          node {
            quizName
            rounds {
              instructions
              name
              questions {
                answer
                image
                question
                audio
                video
              }
            }
            date
          }
        }
      }

      allQuizzesYaml(sort: { fields: date, order: ASC }) {
        edges {
          node {
            quizName
            rounds {
              instructions
              name
              questions {
                answer
                image
                question
                audio
                video
              }
            }
            date
          }
        }
      }
    }
  `);
  const quizArray = [
    ...data.allQuizzesJson.edges,
    ...data.allQuizzesYaml.edges,
  ].map((edge) => edge.node);
  return quizArray;
};

export const useExamples = () => {
  const quizArray = useQuizzes();
  const totalQuestions = quizArray
    .flatMap((quiz) => quiz.rounds)
    .flatMap((round) => round.questions).length;

  const allRounds = quizArray
    .flatMap((quiz) => quiz.rounds)
    .map((round) => round.name);
  return { examples: twoElementsToCompare(allRounds), totalQuestions };
};
