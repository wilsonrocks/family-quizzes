const randomIndex = (arr) => Math.floor(Math.random() * arr.length);

const randomElement = (arr) => {
  const randomIndex = Math.floor(Math.random() * arr.length);
  return arr[randomIndex];
};

const splitIntoWords = (str) => str.split(' ').map((str) => str.toLowerCase());

export const twoElementsToCompare = (arr) => {
  const firstElement = arr[randomIndex(arr)];
  const firstWords = splitIntoWords(firstElement);
  const acceptableSecondWords = arr.filter((roundName) => {
    return splitIntoWords(roundName).every((word) => {
      return !firstWords.includes(word);
    });
  });

  const secondElement =
    acceptableSecondWords[randomIndex(acceptableSecondWords)];
  return [firstElement, secondElement];
};
