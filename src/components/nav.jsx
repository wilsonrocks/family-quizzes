import React, { useEffect } from 'react';
import { Link, navigate } from 'gatsby';

const Navigation = ({ backUrl, nextUrl, holdKeyboardNavigation = false }) => {
  const listener = (event) => {
    if (event.key === ' ' && !holdKeyboardNavigation) navigate(nextUrl);
  };
  useEffect(() => {
    document.addEventListener('keypress', listener);

    return () => {
      document.removeEventListener('keypress', listener);
    };
  }, [listener, holdKeyboardNavigation]);

  return (
    <>
      {backUrl ? <Link to={backUrl}>Back</Link> : <span />}

      <Link to="/">Home</Link>

      <Link to={nextUrl}>Next</Link>
    </>
  );
};

export default Navigation;
