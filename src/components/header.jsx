import React from 'react';

const Header = ({ children }) => (
  <header>
    <div>{children}</div>
  </header>
);

export default Header;
