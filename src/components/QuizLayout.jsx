import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 97vw;
  height: 97vh;
  margin: auto;
  place-items: center;
  display: grid;
  grid-template-rows: 50px 1fr 100px;
  grid-template-areas:
    'navigation'
    'quiz'
    'information';
`;

const QuizLayout = ({ children }) => <Wrapper>{children}</Wrapper>;

export default QuizLayout;
