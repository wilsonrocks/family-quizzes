import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';
import styled from 'styled-components';
import { BIG_FLUID, SITE_WIDTH, SMALL_FLUID } from '../constants';
import { SiteNav } from './SiteNav';
import { useExamples } from '../helpers/examples';

const Container = styled.div`
  max-width: ${SITE_WIDTH};
  margin: auto;
  padding-left: 1rem;
  padding-right: 1rem;
`;

const Header = styled.div`
  position: relative;
`;

const Inset = styled.div`
  max-width: 70%;
  position: absolute;
  top: 5%;
  right: 0;
`;

const Title = styled.h1`
  text-align: right;
  font-size: ${BIG_FLUID};
  margin: 0;
`;

const Tag = styled.p`
  margin-top: 0;
  text-align: right;
  font-size: ${SMALL_FLUID};
`;

const PageLayout = ({ children }) => {
  const {
    examples: [firstExample, secondExample],
    totalQuestions,
  } = useExamples();

  return (
    <Container>
      <Header>
        <StaticImage
          loading="eager"
          src="./sea.png"
          width={1000}
          alt="A quizzical looking stick-man being eaten by a giant shark, while a large-toothed sawfish swims in the background."
        />
        <Inset>
          <Title>A Quiz-organised Mess</Title>
          <Tag>
            {totalQuestions.toLocaleString()} questions on topics ranging from{' '}
            <em>{firstExample}</em> to <em>{secondExample}</em>.
          </Tag>
        </Inset>
      </Header>
      <SiteNav />
      {children}
    </Container>
  );
};

export default PageLayout;
