// import React from 'react';
import styled from 'styled-components';

const QuizContent = styled.div`
  grid-area: quiz;
  min-height: 0;
  max-height: 100;
  display: grid;
  place-items: center;
  grid-template-columns: 1fr 1fr;
  width: 100%;
  height: 100%;
  font-size: 2.5rem;
  > * {
    min-height: 0;
    max-height: 100%;
    max-width: 100%;
    text-align: center;
    :only-child {
      grid-column: 1/3;
      grid-row: 1/3;
    }
  }
`;

export default QuizContent;
