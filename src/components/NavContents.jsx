import styled from 'styled-components';

const NavContents = styled.div`
  width: 100%;
  grid-area: navigation;
  display: flex;
  justify-content: space-evenly;
`;

export default NavContents;
