import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

const Nav = styled.nav`
  display: flex;
  justify-content: space-between;
  font-size: 1.5rem;
  font-weight: 700;
  margin-bottom: 3rem;
  a {
    text-decoration: none;
  }
`;

export const SiteNav = () => (
  <Nav>
    <Link to="/">Home</Link>
    <Link to="/quizzes">Quizzes</Link>
    <Link to="/how-to-use">How to use</Link>
    <Link to="/about">About</Link>
    <Link to="/contact">Contact</Link>
  </Nav>
);
