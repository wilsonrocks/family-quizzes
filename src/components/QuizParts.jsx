import styled from 'styled-components';

export const ImageHolder = styled.div`
  img {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`;
