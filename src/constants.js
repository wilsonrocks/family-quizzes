export const SITE_WIDTH = '1000px';
export const SMALLEST_WIDTH = '375px';

export const BIG_FLUID = 'clamp(2rem, 7vw, 3.5rem)';
export const SMALL_FLUID = 'clamp(0.75rem, 3vw, 1.5rem)';
