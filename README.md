# Quizorganised Mess - a personal project

## About

Takes JSON, audio, image and video and outputs quizzes.

This was a site I set up to run a zoom quiz during the first UK coronavirus lockdown.

It was also an opportunity to learn more about gatsby and graphql.

The CSS is **not** my best work.

## Try it Online

[https://quizorganised-mess.netlify.app/](https://quizorganised-mess.netlify.app/)

It is intended to be used on a desktop while screen-sharing over zoom. It just about works on a phone.

## What I learned

Gatsby is _very_ powerful. I'm not sure I've implemented the quiz generation stuff in the most efficient way.

GraphQl is cool - I can see why it's useful on projects with a lot of different data sources and various frontends that need to access them.

Coming up with 30 quiz questions every week, including 5 audio and 10 picture questions is _hard work_!

## Try it

```bash
yarn
npx gatsby develop # dev server on port
npx gatsby build # build a production version
```

## Copyright

I make **NO** claim to copyright of the assets involved in this quiz and will be happy to remove them if anybody complains.
